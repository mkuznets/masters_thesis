MAIN  = main
FIGURES = $(patsubst %.eps,%.pdf,$(wildcard ./figures/*.eps))

MAKEFLAGS += "-j `nproc`"
.PHONY: all softclean clean distclean

all: $(MAIN).tex $(FIGURES)
	# Remove a bunch of cache possibly left from epspdf.
	rm -rf figures/luatex*
	# Compile thesis.
	latexmk -pdf -pdflatex="pdflatex -halt-on-error" $(MAIN).tex
	# Regenerate README from the template.
	m4 misc/README.m4 > README.md

%.pdf: %.eps
	epspdf $<

softclean:
	rm -rf figures/luatex*
	find -iregex '.*\.\(bbl\|blg\|aux\|log\|lof\|loc\|lot\|loa\|out\|toc\|dvi\|fdb_latexmk\|fls\)$$' -type f -delete

clean: softclean
	find -iregex '.*\.\(synctex\|idx\|ilg\|ind\)$$' -type f -delete
	
distclean: clean
	find . -name "*.pdf" -type f -delete