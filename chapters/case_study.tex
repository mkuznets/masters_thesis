\section{Overview}

In order to demonstrate the adaptive facilities of AstraKahn, it is convenient
to use a computational problem which, after straightforward domain
decomposition, exhibits imbalances of the workload assigned to different
processors. Such problems require problem-specific parallelisation patterns and
load balancing techniques to achieve sufficient hardware utilisation.

The Particle-in-Cell (PIC) method \cite{plasma-physics-simulations}, which is
often used in plasma physics for modelling the motion of charged particles in
electromagnetic fields, has the described property. The main challenge is that
balancing computations on particles leads to a large communication overhead due
to poor data locality, which may cancel out the benefits of load balancing.
Consequently, modern parallelisation techniques for PIC inevitably suffer from a
parametric trade-off between the computation imbalance and communication
overhead. The choice of parameters that ensure optimal performance is usually
the matter of heuristics or manual tuning.

\section{The Problem}

Consider a 1-dimensional plasma consisting of $N_p$ electrons and $N_p$ ions with
no external fields applied. Since ions are much more massive than electrons, we
can assume $m_{i}/m_{e} \to \infty$, neglect the motion of ions, and treat them
as a uniform neutralising background. Our aim is to study the motion of the
electrons in the electrostatic field.

The particles follow the Newton-Lorentz equations of motion:
\begin{equation}
  \label{pic:motion}
  \begin{gathered}
  m_e \frac{d v_i}{dt} = \frac{q_e E(x_i)}{m_e} \\
  \frac{d x_i}{dt} = v_i
  \end{gathered}
\end{equation}

where $x_i$ and $v_i$ are the coordinate and velocity of the $i$th particle
respectively. From Maxwell's equations it follows that
\begin{equation}
  \label{pic:epotential}
  E(x) = -\pd{\varphi(x)}{x}
\end{equation}
\begin{equation}
  \label{pic:gauss}
  \pd{E(x)}{x} = \frac{\rho(x)}{\varepsilon_0}
\end{equation}

Combination of \eqref{pic:epotential} and \eqref{pic:gauss} results in Poisson's
equation:

\begin{equation}
  \label{pic:poisson}
  \pd{^2 \varphi(x)}{x^2} = -\frac{\rho(x)}{\varepsilon_0}
\end{equation}

where $\rho$ is the charge density. Hence, given a spatial charge distribution,
one can compute electric field by solving \eqref{pic:poisson} and
\eqref{pic:epotential}, and then solve the equations of motion
\eqref{pic:motion}.

\section{Particle-in-Cell}

Particle-in-Cell (PIC) is a method for numerical solution of the equations from
the previous section. Consider a 1D plasma within the domain $0 \leq x \leq L$
represented by a grid $\{x_j, j \in [0, N_g)\}$ of equidistant points:
$$
x_j = j \Delta x, ~~ \Delta x = L/N_g
$$

$N_p$ particles are described as a set of coordinate-velocity pairs $\{(r_i,
v_i), i \in [0, N_p)\}$. Normally that $N_g \ll N_p$. The evolution of the
system is computed in discrete time with a specified time-step $\Delta t$. In
what follows we assume the charge and mass of electron, reference charge
density, and electric constant to be equal to 1. This can be done by choosing an
appropriate normalisation.

The following four stages are performed sequentially for each time-step:

\begin{itemize}

\item \textbf{Scatter.} Charge densities on grid points are computed from the
particle positions by the following weighting:
\begin{equation}
  \label{eq:charge_density}
  \rho_j = \frac{N_p}{\Delta x} \sum_{i=1}^{N_p} W(r_i - x_j)
\end{equation}

We assume $W(x)$ to be a linear function\footnote{The choice of $W(x)$
correspond to the model called \textit{cloud-in-cell} in which particles
contribute to the charge density only at the nearest grid points. The
contribution to the density value is proportional to the distance from the
particle to the nearest grid point. This can be thought of as the density of
finite uniformly charged cloud.}
\begin{equation}
  \label{eq:particle_shape}
  W(x) =
  \begin{cases}
  (1 - \abs{x})/\Delta x, & \abs{x}/\Delta x < 1 \\
  0,           & \abs{x}/\Delta x \geq 1
  \end{cases}
\end{equation}

% TODO
% What is the difference between \delta x and  x_{j+1) - x_j?

Therefore, a particle with the coordinate
$r_i: x_j \leq r_i \leq x_{j+1}$ contributes to the charge density as follows:
\begin{equation}
  \label{eq:scatter}
  \boxed {
    \begin{gathered}
      \rho_j = \rho_j + \frac{1}{\Delta x} \left( \frac{x_{j+1} - r_i}{x_{j+1} - x_j}\right) \\
      \rho_{j+1} = \rho_{j+1} + \frac{1}{\Delta x} \left( \frac{r_i - x_j}{x_{j+1} - x_j}\right)
    \end{gathered}
  }
\end{equation}

% TODO
%\textbf{TODO}: ion charge density

\item \textbf{Field Solve.} The Poisson equation \eqref{pic:poisson} can be
written in finite-difference form:
$$
  \frac{\varphi_{j-1} - 2\varphi_j + \varphi_{j+1}}{\Delta x} = -\rho_j
$$

In order to solve it we use iterative Gauss-Seidel method:
\begin{equation}
  \boxed{
    \label{eq:field_solve_phi}
    \varphi^n_j = \frac{1}{2} \brs{\varphi^n_{j-1} + \varphi^{n-1}_{j+1} + \rho_j (\Delta x)^2}
  }
\end{equation}

Then $E$ is immediately obtained from \eqref{pic:epotential}:
\begin{equation}
  \label{eq:field_solve_e}
  \boxed{
    E_j = \frac{\varphi_{i-1} - \varphi_{i+1}}{\Delta x}
  }
\end{equation}

\item \textbf{Gather.} In order to compute electric field acting on particles
from the values on grid points the same weighting as in \textbf{Scatter} is
used:
$$
E_i = \sum_{j=1}^{N_g} E_j W(x_j - r_i)
$$

Using the weighting function $W$ from \eqref{eq:particle_shape} we obtain that
only two neighbouring grid points (i.e. $x_j$ and $x_{j+1}$ such that $x_j \leq
r_i \leq x_{j+1}$) contribute to the electric field acting on a particle. The
resulting field is computed as follows:
\begin{equation}
  \label{eq:gather}
  \boxed {
    E_i = \brc{\frac{x_{j+1} - r_i}{x_{j+1} - x_j}} E_j
          + \brc{\frac{r_i - x_j}{x_{j+1} - x_j}} E_{j+1}
  }
\end{equation}

\item \textbf{Push.} Motion equations \eqref{pic:motion} can be numerically
integrated with so-called \textit{leap-frog} method:
\begin{equation}
  \label{eq:push}
  \boxed {
    \begin{gathered}
    r^{t + \Delta t}_i = r^{t}_i + v^{t + \Delta t / 2}_i \Delta t \\
    v^{t + 3 \Delta t / 2}_i = v^{t + \Delta t / 2}_i +  E \Delta t
    \end{gathered}
  }
\end{equation}

% TODO
% -\Delta t or -\Delta t/2 ?
In order to compute the values at $t=0$, $v^{-\Delta t}$ must be computed first
from electric field computed at the initial point in time.

\end{itemize}

The stages of a PIC iteration and dependencies between the grip and particles
are outlined in \autoref{fig:pic_iteration}.

\begin{center}
  \begin{minipage}[c]{\linewidth}
  \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=10cm]{pic_iteration}
    }
  \captionof{figure}{An iteration of the PIC method.}
  \label{fig:pic_iteration}
  \end{minipage}
\end{center}

\section{Parallelisation}

\subsection{Overview}

The PIC uses two main data structures: a numerical grid and an array of
particles' quantities. Since both of them are used on each time-step either
severally or jointly, it is difficult to obtain a decomposition of the problem
that allows balanced parallel computations with minimal overhead. An overview of
existing parallel strategies for PIC is presented in
\cite{parallel-pic-approaches}. In this section the main problems and trade-offs
of PIC parallelisation are outlined.

First, in the Scatter and Gather the quantities from particles are used to
compute values on corresponding grid points, and vice versa. In order to
minimise communication between processors at each time-step, grid points should
be placed locally with corresponding particles:

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=12cm]{grid-particle}
    }
  \captionof{figure}{Grid-particle locality.}
  \label{fig:grid-particle}
  \end{minipage}
\end{center}

That said, communication is still required
since boundary particles contribute to the adjacent grid points from different
processors. Furthermore, since those particles are moving, some effort is
required to preserve the grid-particle locality. For example, given a static
grid partition, particles that are pushed beyond a grid boundaries must be
immediately moved to adjacent processors.


Apart from the communication overhead, there is a load-balancing issue. Although
Push is the most expensive stage of an iteration, the decomposition based purely
on particles would violate the desirable grid-particle locality. On the other
hand, an irregular grid partition with the grid-particle locality preserved and
a nearly even particle distribution will unlikely result in balanced
computations because of the motion of particles. In this case an effort has to
be made either toward prediction of the plasma evolution or dynamic
reconfiguration of the partition.

As a case study we chose the parallelisation strategy developed in
\cite{sandia-pic-load-balance}, which is based on a static grid partition with
the grid-particle locality preserved and runtime load-balancing. The
following sections describe this strategy and discuss its implementation in
AstraKahn with regard to the runtime adaptive mechanisms provided by the
language.

\subsection{Decomposition}
\label{ss:decomposition}

Consider the 1D PIC problem defined in \label{s:particle-in-cell}. Split the
problem into $k$ subproblems as follows:
\begin{itemize}
  \item the grid is divided into $k$ non-overlapping regions with nearly equal
        numbers of consecutive grid points;
  \item each region is augmented with the corresponding particles: for a region
        $G = (x_s, ... , x_e)$ the particles
        $P = \{(r_i, v_i) ~ | ~ x_s \leq r_i < x_e + \delta x \}$ are selected,
        i.e. particles located between two adjacent regions go to the left one:

        \begin{center}
          \begin{minipage}[c]{.8\linewidth}
            \centering
            \adjustbox{valign=t}{%
              \includegraphics[width=8cm]{decomposition}
            }
          \captionof{figure}{Particles partition.}
          \label{fig:particles-partition}
          \end{minipage}
        \end{center}

\end{itemize}


\subsection{Communication}
\label{ss:communication}

Although the subproblems can be processed in parallel at each of the PIC stages,
they are not fully independent. This requires the following communication steps
to be carried out:

\begin{itemize}
\item \textbf{Scatter and Gather:} The subproblems possess the grid-particle
      locality, i.e. they only need to communicate the boundary grid points and
      particles:

      \begin{center}
        \begin{minipage}[c]{.8\linewidth}
          \centering
          \adjustbox{valign=t}{%
            \includegraphics[width=10cm]{comm_scatter}
          }
        \captionof{figure}{Communication after Scatter.}
        \label{fig:comm_scatter}
        \end{minipage}
      \end{center}

\item \textbf{Push:} Communication is needed to preserve the grid-particle
      locality: the particles pushed outside a region are sent into its adjacent
      regions:
      \begin{center}
        \begin{minipage}[c]{.8\linewidth}
          \centering
          \adjustbox{valign=t}{%
            \includegraphics[width=10cm]{comm_push}
          }
        \captionof{figure}{Communication after Push.}
        \label{fig:comm_push}
        \end{minipage}
      \end{center}

\item \textbf{Field Solve:} The three point stencil used in the Gauss-Seidel
      method (see \eqref{eq:field_solve_phi}) requires the regions to maintain
      ghost-points and send their values to the corresponding adjacent regions:
      \begin{center}
        \begin{minipage}[c]{.8\linewidth}
          \centering
          \adjustbox{valign=t}{%
            \includegraphics[width=7cm]{comm_field}
          }
        \captionof{figure}{Communication after Field Solve.}
        \label{fig:comm_field}
        \end{minipage}
      \end{center}

\end{itemize}

\subsection{Load-Balancing}
\label{ss:load_balance}

Since the grid partition is regular, the computation at the Field Solve stage is
always balanced. By contrast, the computational balance of the other three
stages depends on the distribution of particles among the grid regions. The
distribution is governed by particle motion and can not be guaranteed to be
even.

In order to balance the computations there is a mechanism for particle
migration: particles from ``overpopulated'' processors can by dynamically
delegated to ``underpopulated'' ones via \textit{windows}. A window is a
contiguous block of grid points that is assigned to the other processor
(\autoref{fig:particles-windows}). The windows do not affect the Field Solve
that is performed on the original regions, whereas Gather, Push, and Scatter
stages are performed on the windows by the assigned processor.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=12cm]{windows}
    }
  \captionof{figure}{Particle windows assigned to other processors.}
  \label{fig:particles-windows}
  \end{minipage}
\end{center}

Given an appropriate delegation strategy, the mechanism effectively balances the
particles among processors. However, it introduces a new trade-off between the
migration overhead and particle imbalance. The mechanism provides a user-defined
parameter representing a threshold of particle imbalance that triggers the
balancing procedure.

\section{Implementation in AstraKahn}

\subsection{Sequential PIC}

A sequential PIC problem can naturally be represented in AstraKahn as a serial
replication of the four described stages connected in a pipeline:

\begin{center}\begin{minipage}{0.8\textwidth}
\centering
\begin{alltt}
(scatter .. poisson* .. ef_solve .. gather .. push)*
\end{alltt}
\end{minipage}\end{center}

\noindent
where the Field Solve stage is split into two separate vertices standing for the
Poisson and electric field solvers.

Provided that an input message contains a grid $G$, particle array $P$, and
necessary scalar quantities, each vertex in the pipeline can be implemented as a
transductor that performs computations on the grid or particles and which sends
the resulting message.

Assume that the reverse and forward fixed points in the serial replications are
such that the \texttt{poisson} vertex is replicated until the error becomes
sufficiently small, and the whole pipeline is replicated a specified number of
times corresponding to the desired simulation time.


\subsection{Parallel PIC}

\subsubsection*{Fragmentation}

In order to enable parallelism in the network we need to augment the vertices
with corresponding morphisms that split the initial message into a number of
smaller ones and which join them back to obtain the result. Furthermore, the
transductors representing PIC stages must be programmed to be able to process
``fragments'' of an initial problem.

The morphism's inductor is the same for all stages and follows the decomposition
strategy described in \autoref{ss:decomposition}. In addition, since Gather,
Scatter, and Field Solve use the adjacent grid points from the neighbouring
regions, two \textit{ghost points} are attached to each
message\footnote{Hereinafter we omit the message data that is not involved in
the algorithm in question (e.g. $\Delta x$, $\Delta h$, the number of grid
points, etc.). We assume the data to be inherited from the input messages as
appropriate.} (\autoref{alg:morph_split}).
\input{algorithms/morph_split}

By contrast, the morphism's reductor is stage-specific because it has to perform
communication between subproblems. However, instead of writing five separate
reductors, we compose each of them out of three boxes two of which are common
for all stages:

\begin{itemize}
  \item The first box is a reductor that simply combines the regions,
        corresponding particle arrays, and ghost points without performing any
        operations on them (\autoref{alg:morph_combine}).
  \item The next box is the transductor that performs stage specific
        communications as described in \autoref{ss:communication}. For example,
        for the Scatter stage it adds the partial density from the right ghost
        cells to their ``actual'' counterparts in the neighbouring regions.
        (\autoref{alg:morph_scatter}).
  \item Finally, the last box is also a transductor that joins the subproblems
        into a single grid and particle array (\autoref{alg:morph_assemble}).
\end{itemize}

Putting it all together, the five morphisms are constructed as follows:
\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=9cm]{pic_morphisms}
    }
  \captionof{figure}{Parallel PIC morphisms.}
  \label{fig:pic-morphisms}
  \end{minipage}
\end{center}

\input{algorithms/morph_combine}
\vspace*{-.6cm}
\input{algorithms/morph_scatter_xchg}
\vspace*{-.6cm}
\input{algorithms/morph_assemble}

\subsubsection*{Overrides}

In order to avoid global synchronisation after each stage, appropriate overrides
have to be specified for each connected pair of stages. Since each subproblem
communicates only with its immediate neighbours, the data can be efficiently
transferred by synch-tables. As an example, consider an override between
\texttt{scatter} and \texttt{poisson}, a similar approach applies also to the
rest of the morphism pairs.

As follows from \autoref{alg:morph_split}, the right ghost point of a region has
to be sent to its right neighbour. This can be done by extending the scatter's
transductor with a separate output channel, on which the right ghost point is
sent. Then the messages go to the synch-table that routes them in such a way
that the ghost point and the corresponding neighbouring region are sent to the
same synchroniser. The synchroniser in turn combines the messages and sends the
result to the next stage (\autoref{fig:pic-override}).

However, for the communication to be completed the density value from the
attached ghost point has to be added to the leftmost point of the grid. This can
be done by adding an auxiliary transductor before the one in the
\texttt{poisson} morphism.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=8cm]{override_output}
    }
  \captionof{figure}{Override between \texttt{scatter} and \texttt{poisson}
                     stages.}
  \label{fig:pic-override}
  \end{minipage}
\end{center}

\subsubsection*{Load balancing}

Consider the PIC load balancing method discussed in \autoref{ss:load_balance}.
In AstraKahn terms the window assignment corresponds to copying grid- and
particle-data within the selected window from one message to another. Since the
decision about window creation is made at runtime and collectively for all
subproblems, it is convenient to embed load balancing into the fragmentation
mechanism.

Let each morphism's inductor compute the particle distribution among the
subproblems. If the particle imbalance exceeds a (sufficiently low) predefined
threshold, a global set of windows is computed, and each generating message is
augmented with sets of ``incoming'' (assigned \textit{from} other messages) and
``outcoming'' (assigned \textit{to} other messages) windows:
$$
\begin{gathered}
W^{in}_i = \{(G^i_j, P^i_j) ~|~ j \in S_{in} \subseteq \{1..k\} \backslash i \} \\
W^{out}_i = \{(G^j_i, P^j_i) ~|~ j \in S_{out} \subseteq \{1..k\} \backslash i \}
\end{gathered}
$$
where $(G^j_i, P^j_i)$ is a window of the $j$th message assigned to the $i$th
one.

Transductors for \texttt{gather}, \texttt{push}, and
\texttt{scatter} have to be programmed to work within the grid region
$\left( G_i \backslash \{G^j_i ~|~ j \in S_{out} \} \right)
            \cup \{G^i_j ~|~ j \in S_{in} \}$.
This creates additional communication points: as a normal region, a window
contains particles that depend on the leftmost point of the adjacent region (see
\autoref{fig:comm_scatter}). Furthermore, since the electric field is computed
regardless of the windows, the \texttt{scatter}--\texttt{poisson} and
\texttt{ef\_solve}--\texttt{gather} overrides have exchange the charge density
and electric field between messages and their ``outcoming'' windows.

Now the runtime system can perform the load balancing procedure by
``refragmenting'' the messages. It does so once an imbalance is detected in the
execution times of parallel boxes at Gather, Push, or Scatter stages. The
threshold of the imbalance is controlled by self-tuning mechanism.