\section{Overview}

In \autoref{s:compound} we defined two regulating actions in AstraKahn that are
managed by the coordinator:
\begin{itemize}
  \itemsep0em
  \item \textit{Proliferation} replaces a primitive box by its parallel
        counterpart consisting of several copies of the original box. The number
        parallel copies is called the \textit{proliferation factor}.
  \item \textit{Fragmentation} breaks a single input message into a number of
        smaller ones by a morphism assigned to some transductor.
\end{itemize}

The coordinator applies these actions to an AstraKahn network at runtime in an
attempt to improve its performance characteristics. The actions are coupled:
fragmentation generates a large supply of messages, which can be processed in
parallel by applying proliferation to boxes. This allows one to increases the
throughput of individual boxes in the network and, possibly, decrease the
overall processing time.

There are also two complementary actions: \textit{deproliferation}, which
decrease the proliferation factor for some boxes in favour of the ones with a
higher computational demand, and \textit{refragmentation}, which joins the
generated messages into one and splits again with a different number of
fragments.

However, under limited computational resources the positive effect of these
actions has a certain upper bound. Hence, the coordinator must have sufficient
intelligence to regulate the network in accordance with its intended behaviour
and the computational demand. In order to do this the AstraKahn coordinator
continually performs the optimisation loop:
\begin{itemize}
  \itemsep0em
  \item monitor runtime characteristics of each vertex;
  \item use built-in regulation strategies together with currently and
        previously observed properties to produce a new set of regulating actions;
  \item save regulating parameters for further use.
\end{itemize}

We refer to this form of runtime adaptivity as \textit{self-tuning}. In the
following sections we outline a number of possible regulating strategies.


\section{Framework}

Let an AstraKahn network implement some algorithm that admits divide-and-conquer
strategy. The network receives a message, performs computations, and yields the
result as a single message. Our objective is to minimise the overall running
time, or, in other words, to minimise the \textit{latency} of this network.

We focus on the networks that are primarily serially connected and consist
mainly of transductors. This particular choice was made since a series of
independent computational steps is a common pattern in algorithm design, and
since AstraKahn transductors are designed to express primary computations in a
program. Hereinafter we refer to the network as a \textit{pipeline}, and to its
transductors as \textit{stages}.


At the beginning the coordinator is unaware of the network's runtime properties.
In order to collect a substantial amount of profile information on the network,
assume that either
\begin{itemize}
  \itemsep0em
  \item a program consists of a single pipeline and can be run several times;
  \item or the pipeline is serially replicated.
\end{itemize}

In either case the network's runtime profile is accumulated and specified in
order to obtain more precise behavioural and performance model.

The proposed scheme is contingent on the availability of a large number of
messages floating between pipeline stages. This could be a feature of the
application defined in its natural form or the messages can be the result of
applying a morphism, i.e. fragmentation.

For the fragmentation to be applied over the whole length of the pipeline, as
opposed to a single transductor, each transductor is augmented with a morphism.
Assume also that appropriate overrides are specified for each adjacent pair of
morphisms. This requirement is essential in order to avoid global
synchronisation caused by an explicit join-split in the middle of the pipeline.

% $$
% pic A
% $$
% The resulting network is depicted in Figure~A.

Initially fragmentation is applied to the first stage of the pipeline, and an
input message is split into a sequence of fragments that continue travelling
along the pipeline. For a given point in time there is a number of ``active''
stages, i.e. the ones in which messages are being processed. We refer to this
number as \textit{effective pipeline depth}.

If there is no data dependency between the messages, the depth can potentially
be equal to the whole length of the pipeline. However, it is often the case that
a message depends on several other messages, since they are combined in
overrides. Accordingly, the effective depth of the pipeline can be limited as
well.

The main open issue with proliferation and fragmentation is one of predictive
control. In a situation when distributed resources are limited, the decision to
proliferate is based on an expectation of parallel speedup, future
availability of computing cores, and communication constraints. Any predictive
rule will necessarily have to take into account the resource footprint of each
box including its dependencies on the content of messages circulating in the
system. Additionally, since messages can be fragmented, a proliferation rule is
dependent on the behaviour of the fragmentation mechanisms. It has to determine
that proliferation is profitable despite the fragmentation overheads, as well as
ascertaining the degree of the fragmentation.

\section{Fragmentation}

First, consider the initial message fragmentation. The number of fragments $m$
has to be sufficient to use the available parallel resources. On the other hand,
if $m$ becomes too large, the processing time of an individual message becomes
very short, while the communication overhead goes up as well. The communication
overhead could eventually predominate over the parallel speedup. Additionally,
the multitude of messages has the potential to overwhelm the buffering capacity
of channels if the boxes receiving data from them prove too slow.

For a reasonable initial guess of $m$ we can assume that
the maximum effective depth is equal to the full length of the pipeline, and
that the processing rate of all the stages is the same; as a result, all the
stages will be active the whole time. Under such circumstances, the
fragmentation will produce $m = \sum_{i=1}^n c_i / 2$ messages with the
intention to load at least half of the channel's capacity at each stage. Later on,
when the pipeline depth and other monitoring parameters are collected, $m$ will be
adjusted accordingly.

During the execution, if the communication overhead starts to be significant, or
if the effective depth starts to gradually decrease due to a blocked channel,
the number of messages will be reduced; and vice versa, if the parallel resources are
not fully used during the execution, the initial fragmentation factor will be
increased.

Here we assume that refragmentation is performed only between consecutive
replicas of a serial replication, or not at all. It is simpler for the
implementation since to apply refragmentation at an arbitrary point of the
pipeline the runtime system has to make sure that the ``front'' of the message
sequence is behind the vertex where the refragmentation is going to be made.

\section{Proliferation}

Proliferation replaces a single transductor by a number of copies. Such a
replacement can potentially increase the performance provided that the copies
operate in parallel. Parallel operation is not guaranteed: boxes are not
assigned to the hardware cores, therefore if the number of boxes is greater than
that of the available cores, the effect on the throughput depends on the quality
of the scheduling.

However, assume that the effect from the parallel transductor is close to
linear. This can be achieved for a relatively small network and sufficient
number of cores. The resource limit is in terms of the maximum number of
transductor copies that can be spawned. Assume also that each stage of the
pipeline process messages with some throughput. Note that the running time of
the pipeline is equal to its throughput multiplied by the number of messages.
Hence, the throughput of the pipeline is equal to the minimum throughput of
individual stages. Now the self-tuning of the pipeline boils down to the
maximisation of the minimum throughput in the pipeline subject to the limited
number of parallel boxes, which can be written formally as a max-min linear
optimisation problem. The problem can easily be solved provided that the
throughput of each stage is know \cite{Maximin}. However, the throughputs of the
stages can only be determined at runtime, by observing the stages. In
\cite{TowardsSelfAdaptive} a similar problem is solved for a pipeline in a
steady state by monitoring the execution time of its stages and recalculating
the proliferation factors after each new run.

The AstraKahn pipeline in question can not be assumed to be in a steady state
since in the case of a small effective pipeline depth the stages work non-
uniformly: only a small number of them are active at any given time. However,
by choosing an appropriate time-scale the active stages can be considered to be
in a steady state.

Let a pipeline of length $L$ process all messages from a fragmented input
sequence for time $T$. Assume the at the time $t$ there are $d(t) \ll L$ stages
are active for a period of time $T_s \leq T$. Within the period we can consider
the sub-pipeline consisting of $d(t)$ stages to be in a steady state for
$T_s$. Let $T_p$ denote the time that it takes to process the observed
properties of stages and reassign the proliferation factors. If $T_p \ll T_s$
(which can be tested at runtime as well), the runtime system can effectively
solve the optimisation problem for the active pipeline stages achieving optimal
parallel resource mapping from that time on.

The above approach is based on monitoring the properties of the stages and
generating a short-term prediction that expectedly meets the constraints of the
network. There is another strategy in which the decision about proliferation is
made individually by each of the stages based on local observations. The
approach can be called \textit{greedy} since each stage tries to optimise its
local problem, in the hope that their collective choice will be optimal for
the whole network.

For example, as before, assume that there is a global ``pool'' with $M$ parallel
transductors. Let a stage of the pipeline create a new parallel transductor on
receiving a message provided that the pool is not empty. In other words, the
stage always tries to process incoming messages in parallel subject to the
available resources. In order to prevent monopolising parallel resources, the
stage also ``returns'' parallel transductors back to the pool when the
computational demand of the stage has decreased. For example, it can reduce its
proliferation factor as soon as it is idle for an average inter{-}arrival time
at the moment that the parallel transductor is taken from the pool.

% $$
% pic
% $$

% \section{Box performance model}

% % Need a performance model of boxes
% However, in an important particular case when the workload of messages at each
% stage either not the same even right after the fragmentation, or significantly
% imbalanced during the execution. In these cases it might be