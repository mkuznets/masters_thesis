\label{introbegin}

\section{Coordination Programming}

%-------------------------
% Coordination Programming

% \deleted{Coordination programming is a paradigm that regards a computer
%   program as a number of \textit{computational} components controlled by a
%   \textit{coordination} mechanism that is responsible for execution of the
%   components, as well as their communication and synchronisation.}

% \deleted{
%   Provided that the coordination and computational parts are separate to some
%   extent, the paradigm moves the concerns of concurrent execution away from the
%   computational semantics of the program. From the pragmatic point of view this
%   allows the computational components to be universal and portable since they
%   can potentially be written on any language and migrate between coordination
%   platforms without modifying the tested code.%
% }

\added{
  % 
  Coordination programming is a paradigm for managing composition,
  communication, and synchronisation of concurrent programs, generally called
  \textit{components}. A \textit{coordination language} is a programming
  language for such coordination \cite{arbab1998you}.

}

%-------------------------
% Data-Flow

\added{ 
  While there are several approaches to coordination programming, in this work
  we focus on \textit{dataflow} coordination, in which the execution flow is
  defined in terms of availability and/or mutation of data.
%
}

%-------------------------
% Data-Flow: Tuple Space (Linda, CnC)

\added{
  The approach was implemented first in the coordination language \textit{Linda}
  \cite{Linda} which allows several sequential components to communicate via
  shared associative, concurrently accessed memory storage, called the
  \textit{tuple space}. The components can read, write, and remove tuples of
  objects from the tuple space; they can also wait for a particular tuple and
  resume execution as soon as it appears in the tuple store. The ideas of Linda
  found a new life in a recent project at Intel, called \textit{Concurrent
  Collections} (CnC) \cite{CnCReport, CnCImplementation}. Like Linda, CnC uses
  content-addressable storage called a \textit{tag collection} and supports
  dataflow synchronisation.
%
}

\added{
%-------------------------
% S-Net
  Another possible aggregation mechanism (or \textit{glue} between the
  components) is \textit{streaming networks}. One of the recent representatives
  of the approach is \snet{} \cite{SNet}. Here the concept of coordination is
  fully developed in that the application is represented as two separate
  programs: a collection of \textit{boxes} with well{-}defined interfaces and a
  coordination program written in a specially constructed language that connects
  and coordinates those interfaces. The boxes are single-input single-output
  pure functions of streams written in a conventional language and oblivious to
  concurrency concerns. They are connected into a streaming network which (i)
  has a static topology where the connections are irregular, and (ii) can
  dynamically evolve, but only according to regular patterns where the
  dependencies between boxes remain statically analysable. Each box waits for an
  input message, performs some processing, and outputs zero or more messages
  into its output stream. The boxes are executed concurrently by the S{-}Net
  runtime system.
%
}

\section{Data Parallelism}

\added{
  \snet{} boxes process messages sequentially, one after another. However, in
  some cases it is possible to apply a box to several messages in parallel. In
  \snet{} it can be implemented with an \textit{index split combinator}: a
  stream splitter that specifies an integer tag and creates parallel instances
  of the box for every message with the unique tag (\autoref{fig:indexsplit}).

  \begin{figure}
  \centering
  \includegraphics[width=8cm]{index_split}
  \caption{\snet{}: index split combinator and the resulting network.}
  \label{fig:indexsplit}
  \end{figure}

  This approach requires a programmer to write additional code for extending
  certain messages, which are eligible for parallel processing, with the tag.
  The number of the unique tags defines the number of parallel instances thus
  explicitly regulating the level of parallelism. Furthermore, the combinator
  does not guarantee the order of output messages. If the order is essential,
  the sorting must be performed explicitly.

  Another approach called \textit{concurrent box invocation} was introduced in
  the new \snet{} runtime system \front{} \cite{GijsGrelIJPP14}. The runtime
  system executes each box in a number of parallel contexts activated upon
  receiving input messages. The processed messages from these contexts are sent
  to the collector, where they may be sorted, and then to the output stream
  (\autoref{fig:concinvoc}). For each box the maximum number of parallel
  contexts and the need for message sorting are configured manually.

  \begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{concurrent_invocation}
  \caption{\front: a box is executed in four parallel contexts}
  \label{fig:concinvoc}
  \end{figure}

  The latter approach is less explicit, but it still requires the programmer to
  configure the level of concurrency for each box. The optimal settings may
  strongly depend on the hardware, input data, and/or dynamic properties of
  algorithm implemented by the network.

  The number of concurrently processed messages (or \textit{data granularity})
  is also configured manually. However, it is generally unclear how many
  messages are sufficient to utilise the available resources fully. Too many
  messages may cause an overhead, while having too few of them may prevent
  potential pipeline parallelism.
%
}

\section{AstraKahn}

\added{
  AstraKahn is a new coordination language \cite{AstraKahn} that inherits some
  concepts of \snet{} with a number of refinements. Those include stream
  structuring, classes of boxes, limited capacity channels, and a well{-}defined
  protocol of box execution that prevents a box from holding its state when it
  is blocked. A detailed description of the language is given in
  \autoref{c:astrakahn}.

  AstraKahn provides an implicit kind of box parallelism called
  \textit{proliferation}. It is conceptually similar to the one in \front{}.
  Unlike \front{}, however, the concurrency levels of boxes are expected to be
  set automatically based on the information collected by monitoring the network
  and analysing resource utilisation.

  We refer to this form of runtime adaptivity as \textit{self{-}tuning}. The
  mechanism can also govern the granularity of data parallel problems. Using a
  build-in pattern called \textit{morphism} a programmer can define problem
  decomposition with granularity adjusted at runtime based on the observed
  network performance.

  The morphism pattern may also be useful for problems where a straightforward
  data decomposition results in imbalances of the workload assigned to different
  processors. Normally, specific load{-}balancing techniques are developed for
  these problems in order to benefit from data parallelism. When such techniques
  suffer from the compromise between the negative effect of computation
  imbalance and the overhead of the balancing, one can delegate the decision to
  the runtime system by implementing the problem using morphisms.
%
}

\section{Contributions}


\added{

  The challenge of self-tuning can only be addressed with new heuristics and
  well-chosen assumptions about program behaviour. The first step, however,
  should be to implement the AstraKahn prototype as a test bed on which to run
  an example application while attempting several adaptation strategies. Since
  the language itself is not yet stable, the prototype has to be easily
  extensible. It is such a prototype that the present work is attempting to
  develop.

  The main contributions of the thesis are the following

  \begin{itemize}
    \item An up-to-date definition of AstraKahn is presented
    (\autoref{c:astrakahn}). While the language was first defined by Shafarenko
    in his preliminary report \cite{AstraKahn}, during the course of research a
    number of amendments and clarifications were made. They include network and
    synchroniser description languages, practical representation of morphisms,
    and definition of the fixed point series;
    \item An implementation of AstraKahn compiler and runtime system prototype
    is described (\autoref{c:implementation}). We discuss and motivate a number
    of design decisions. Although the implementation itself is still under
    development, the core language and runtime system components, as well as a
    number of extensions, are completed \cite{AKImpl}.
  \end{itemize}
}

\added{
  In addition, as a minor contribution, a number of potential self-tuning
  heuristics for a simple network pattern have been proposed
  (\autoref{c:self_tuning}). We intend to test them as soon as the AstraKahn
  prototype has been implemented. Also, for illustrative purposes, a practical
  application of morphism pattern is presented. The particle-in-cell problem,
  whose parallelisation requires load-balancing, was formulated in AstraKahn
  using the morphism pattern (\autoref{c:pic}).
%
}

% \section{Outline of the Thesis}

% \added{
%   This document is structured as follows. \autoref{c:astrakahn} introduces the
%   AstraKahn language. Several approaches to the self-tuning problem are outlined
%   in \autoref{c:self_tuning}. \autoref{c:implementation} describes the
%   implementation of an AstraKahn proof{-}of{-}concept prototype. The case study
%   and its implementation in AstraKahn are given in \autoref{c:pic}.
%   \autoref{c:conclusion} summarises the work and lays down plans for further
%   research in the project.
% }

\label{introend}