As a proof-of-concept and a framework for further research and experiments, a
prototype AstraKahn compiler and runtime system have been implemented. This
chapter provides some details on design of the implementation.

The architecture of the \replaced{prototype}{AstraKahn implementation} is given
in \autoref{fig:arch}.
\added{%
  The compiler compiles an AstraKahn program down to a network of
  \textit{runtime components}. Each component is an object (in terms of object
  oriented programming) that corresponds to some AstraKahn vertex by
  implementing unified communication and execution interfaces as appropriate.
  Then the runtime system runs the network, managing communication and parallel
  execution of the components. }

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=10cm]{prototype_updated}
    }
  \captionof{figure}{Architecture of the AstraKahn implementation.}
  \label{fig:arch}
  \end{minipage}
\end{center}

\section{Runtime System}

\subsection{Overview}
\label{ss:rts_overview}

A network of concurrent communicating vertices can be implemented in various
ways. A straightforward one is to represent the vertices as kernel-level threads
or processes communicating via shared memory or other inter-process
communication primitives. \added{This approach was employed in the original
\snet{} runtime system \cite{GrelPencIFL08}.} The drawbacks of the strategy are
the lack of scheduling flexibility (since threads are governed by the OS) and
the high overhead caused by expensive context switching and network
reconfiguration. \deleted{The latter makes this approach hardly useful for
AstraKahn, which strongly relies on dynamic network structures, such as serial
replication and parallel boxes.}

The drawbacks can be avoided by using \added{lightweight} user-level threads.
The LPEL, a runtime layer for\deleted{coordination language} \snet{} \cite{LPEL},
uses this strategy as follows. Vertices of an \snet{} network are represented as
\textit{tasks} distributed among a fixed number of \textit{workers}, which are
persistent kernel-level threads. The tasks can communicate within and across
workers. Once a task is ready, it is executed in the context of its worker
(\autoref{fig:lpel}).

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.51\textwidth}
    \includegraphics[width=\textwidth]{lpel}
    \caption{The LPEL architecture}
    \label{fig:lpel}
  \end{subfigure}%
  \qquad %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
    %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[t]{0.4\textwidth}
    \includegraphics[width=\textwidth]{runtime_conductor}
    \caption{LPEL with a dedicated conductor}
    \label{fig:lpel_conductor}
  \end{subfigure}

  \caption{Runtime systems based in lightweight user-level threads.}
\end{figure}



However, in order to run efficiently, the LPEL has to maintain the load-balance
of the workers by task migration. A strategy of dynamic task migration for the
LPEL is discussed in \cite{taskmigration}. That said, the problem is rather
challenging, and it seems to be an overkill to incorporate it into a research
prototype. \added{Another approach suggested in
\cite{NguyenKirner13ICA3PP} is to assign one worker called \textit{conductor}
to maintain a \textit{central task queue} (CTQ) and dispatch ready tasks to the
\textit{pool} of workers for execution (\autoref{fig:lpel_conductor}).}

\deleted{We avoid the problem by separating the runtime components from workers: the
network components are managed by the runtime system in a single process that
forms tasks (i.e. compounds comprising a box function reference and input
messages) and dispatches those to the \textit{pool} of workers for execution
().}

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=8cm]{runtime_updated}
    }
  \captionof{figure}{The runtime system of AstraKahn.}
  \label{fig:runtime}
  \end{minipage}
\end{center}

An immediate drawback of this approach is the communication load between the
conductor and the workers on each execution of a vertex. However, we assume the
task size to be small: messages usually contain lightweight data such as
integers and references to heavier data objects rather than the objects
themselves. Hence, to the first approximation we can neglect the inefficiencies
associated with communication between the conductor and the workers. Another
potential drawback is bad cache behaviour: closely connected vertices can be
assigned to different workers (and, possibly, to different hardware cores) while
taking turns to process the same message. We also neglect the drawback in the
current implementation; however, the runtime system may account for it by
employing a prioritised policy of scheduling vertices among workers.

We refer to the aforementioned method of execution as \textit{remote}. The
method is used for boxes: their functions and input messages are combined into
tasks and asynchronously sent to the workers.

However, in some cases the remote execution is redundant and may cause a
significant overhead:
\begin{itemize}
\item A segmentation mark being received by a box bypasses its functions and is
      forwarded to its output channels, possibly with amended depth;
\item A synchroniser changes its state on receiving a message, which also
      requires a constant time. Furthermore, while choosing a state transition,
      the synchroniser needs to know the status of its input and output
      channels, which may change while the synchroniser is executing in the
      pool.
\end{itemize}

In these cases the conductor executes the components \textit{immediately},
within the context of the conductor's process (\autoref{fig:runtime}).

\subsection{Runtime Components}
\label{ss:runtime_cmpts}

Runtime components are objects that correspond to vertices and nets. The objects
are structured in a tree that reflects the hierarchy of an AstraKahn program:
its internal nodes are nets, and leaves are vertices
(\autoref{fig:runtime_components}). The tree is constructed by the AstraKahn
compiler (see \autoref{s:compiler}).

\begin{center}
  \begin{minipage}[c]{\linewidth}
    %\centering
    \captionsetup{type=figure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \begin{alltt}
      net n0
        net n1
          (A || C) .. B
        net n2
          D .. E
      (n1 || S) .. n2\end{alltt}
        \caption{Network description (simplified).}
        \label{fig:runtime_net_descr}
    \end{subfigure}
    \hspace{1cm}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=7cm]{components}
        \caption{Tree of runtime components}
        \label{fig:runtime_tree}
    \end{subfigure}
    \caption{Correspondence between AstraKahn program and runtime components}
    \label{fig:runtime_components}
  \end{minipage}
\end{center}

The components have common interfaces for communication and execution.

\subsubsection*{Communication}

Each component has a number of named input and output ports that denote
connection endpoints (see \autoref{s:wiring}). However, the implementation does
not consider a channel as a separate entity. Instead, a bounded buffer is
embedded into each input port. From the standpoint of an output port a
connection is represented as a reference pointing to the buffer of some input
port.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
  \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=8cm]{connection}
    }
  \captionof{figure}{Implementation of AstraKahn channels.}
  \label{fig:channels}
  \end{minipage}
\end{center}

The buffer is implemented as a double-ended queue, i.e. such that allows one to
add or remove elements from either the head or tail. The resulting capability of
components to insert messages into their input channel is used by reductors and
inductors for continuation messages (see \autoref{ss:implementation_boxes}).

In AstraKahn a net is a shorthand for some subnetwork, it neither receives
messages nor computes anything \textit{on its own}. That said, runtime
components representing nets contain explicit placeholders for inputs and output
ports. Normally, these ports are simply references to (or, in fact, ``proxies''
for) corresponding ``real'' ports of net's constituent components.
(\autoref{fig:net_noexec}).

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=8cm]{net_noexec}
    }
  \captionof{figure}
    {Ports of a net that is not involved in execution. Dashed grey lines denote
    references to ports, solid ones -- references to input buffers.}
  \label{fig:net_noexec}
  \end{minipage}
\end{center}


However, the implementation allows some special built-in types of nets to
receive and process messages on their own. Components of this type have a
predefined \textit{handler} that is called by the \replaced{conductor}{runtime
system} once a messages is received by one of the net's ``real'' input ports.
The handler has access to the net's internals, for example, it can add, remove
or rewire any vertices inside the net. It also can forward the received messages
to any input ports of constituent vertices (\autoref{fig:net_exec}).

Note that these non-standard nets are preprogrammed and can only be used for
internal purposes. For example, such a net is used for serial replication wiring
(\autoref{ss:wire_exp}).


\begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{net_exec}
  \caption{Ports of a net involved in execution.}
  \label{fig:net_exec}
\end{figure}

Each node also has a private interface containing methods for fetching and
sending messages, and checking the statuses of input and output channels. These
methods are used by the execution interface.

\subsubsection*{Execution}
\label{ss:execution}

An execution interface of runtime components consists of the following methods:

\begin{itemize}
\item \texttt{is\_ready()} -- returns a tuple of two Boolean values $(R_i,R_o)$
      where $R_i$ indicates availability of input message(s) required to run,
      and $R_o$ indicates whether the output channels are unblocked. The values
      $(\texttt{True},\texttt{True})$ are returned when the component is ready
      for execution.
\item \texttt{fetch()} -- fetches and returns input message(s) required for
      execution.
\item \texttt{run()} -- performs execution of the component. The method returns
      either a task for remote execution or \texttt{True} if immediate execution
      is performed.
\item \texttt{commit()} -- accepts a result computed in the pool and sends it
      to the specified output port(s).
\end{itemize}

The interface is abstract and implemented by each vertex individually. Concrete
interface implementations for all types of vertices are given in
\autoref{s:component_implementation}.

\subsection{Scheduling}
\label{ss:scheduling}

In order to run a network of components, the runtime system performs
\textit{dynamic scheduling}, i.e. computes a set of \textit{ready} components at
runtime. More specifically, the runtime system implements \textit{data-driven
scheduling}, where a component is considered \textit{ready} if it has input
message(s) to process. This approach is easy to implement, suitable for
exploiting parallelism of asynchronous components and, provided that all
channels are bounded, results in fair network execution \cite{parks1995bounded}.
The same approach was also used in the LPEL \cite{LPEL}.

Therefore, the problem of scheduling boils down to effective computation of the
set of ready components at runtime. Before executing a network, the
\replaced{conductor}{runtime system} computes an initial set its ready
components, and then updates it as follows. After each component's execution the
\replaced{conductor}{runtime system} gets a set of components from which the
messages were processed, and components to which output messages were sent.
These components are candidates to become ready due to unblocked output channels
or newly available input messages respectively. As soon as the set of ready
components becomes empty, new candidates that happen to be ready are placed in
it.

\subsection{Network Execution}

After computing an initial set of ready components, the
\replaced{conductor}{runtime system} starts a loop that performs the following
high-level steps:
\begin{enumerate}
  \def\itemautorefname{step}
  \item \label{e:start} pop ready component and run it immediately, or dispatch
        it to pool;
  \item update set of ready components;
  \item \label{e:pool} read results of remote execution from pool and commit
        them;
  \item update set of ready components; if no component is ready, wait
        for further results from pool and repeat
        \autoref{e:pool}; otherwise go to \autoref{e:start}.
\end{enumerate}

Note that the pool is communicating concurrently with the
\replaced{conductor}{runtime system}: dispatching a task to the pool is always
non-blocking, and an operation that reads results from the pool is blocked only
if there is no ready component.

While message communication and immediate execution are performed
non{-}concurrently in the above loop, the overhead is assumed to be small
compared to the running time of boxes executing in the pool. Nevertheless, when
implemented in the runtime system, these operations are top candidates for
profiling and optimisation.

%-------------------------------------------------------------------------------

\section{Runtime Components}
\label{s:component_implementation}

This section describes how AstraKahn vertices are implemented in terms of the
runtime components. Elementary vertices are represented by single components
with an execution interface declared appropriately, whereas built-in extensions
are represented by nets composed of boxes and synchroniser defining the desired
behaviour.

\subsection{Boxes}
\label{ss:implementation_boxes}

\subsubsection{Transductor}

A transductor has the simplest execution interface (\autoref{alg:transductor}):

\vspace{4mm}
\begin{algorithm}[H]
  \input{algorithms/boxes_definitions}
  
  \method{\isReady{}}{
      \tcc{Test if there is an input message and if all output channels are unblocked.}

      \Properties{\input \tcc*[h]{input port} \\
        \outputs \tcc*[h]{array of output ports}} \;

      $R_i$ = \bool{is \input not empty} \;

      $R_o$ = \bool{does \outputs have no blocked channels} \;
      
      \Return{$(R_i, R_o)$}
  }
  \;
  \method{\Commit{\result}}{
      \tcc{Return a task combined of the box function and the input message. If
        the input message is a segmentation mark, it is immediately sent to all
        output channels instead.}

      \ForEach{$($\msg, {\port}$)$ \inn \result}{
          send \msg to \port \;
      }
  }
  \;
  \method{\Run{\msg}}{
      \tcc{Send output messages received from the pool to corresponding
        channels.}

      \eIf{\msg is $\sigma_k$}{
          send $\sigma_k$ to each port in \outputs \;
          
          \Return{\none} \tcc{Immediate execution}
      }
      {
          \Return{$($\self.\function, {\msg}$)$} \tcc{Remote execution}
      }
  }
\caption{Execution interface of transductor.}
\label{alg:transductor}
\end{algorithm}

\subsubsection{Inductor}

In the execution interface of an inductor (\autoref{alg:inductor})
\texttt{is\_ready()} checks for two message slots available on each output
channel since \texttt{run()} sends an additional $\sigma_1$ between output
sequences if there is no other segmentation mark between them. Incoming
segmentation marks are forwarded with the depth increased by one. When a data
message is received, new elements of the output sequence (no more than one
message per output) are computed in the pool and, if the elements are not final,
a continuation message is inserted in the result. When accepted by
\texttt{commit()}, the continuation, if any, is put back to the input channel,
and the computed messages are sent to the output channels.

\vspace{4mm}
\begin{algorithm}[H]
  \input{algorithms/boxes_definitions}
  \method{\isReady{}}{

      \Properties{\input \tcc*[h]{input port} \\
        \outputs \tcc*[h]{array of output ports}} \;

      $R_i$ = \bool{is \input not empty} \;

      $R_o$ = \bool{can each port in \outputs accept two messages} \;
      
      \Return{$(R_i, R_o)$}
  }
  \;
  \method{\Commit{\result}}{
      \Properties{\segflag \tcc*[h]{flag indicating the end of previous sequence generation}} \;

    \eIf{\result contains a continuation}{
      push \result.\continuation back to \input
    }
    {
      \segflag = \true \;
    }

    \ForEach{$($\msg, {\port}$)$ \inn \result}{
        send \msg to \port \;
    }
  }
  \;
  \method{\Run{\msg}}{

    \Properties{\inputs \tcc*[h]{array of input ports},\\
      \segflag \tcc*[h]{flag indicating the end of previous sequence generation} \\
      \outputs \tcc*[h]{array of output ports}, 
      \function \tcc*[h]{box function}} \;

    \eIf{\msg \is $\sigma_k$}{
      $\sigma_s = 
        \begin{cases}
          \sigma_{k+1},~k > 0 \\
          \sigma_0,~k = 0 \\ 
        \end{cases}$ \;

      send $\sigma_s$ to each port in \outputs \;

      \segflag = \false \;

      \Return{\none}
    }
    {
      \If{\self.\segflag}{
        send $\sigma_1$ to each port in \outputs \;

        \segflag = \false \;
      }
      \Return{$($\function, {\msg}$)$}
    }
  }
\caption{Execution interface of inductor.}
\label{alg:inductor}
\end{algorithm}


\subsubsection{Dyadic Reductor}

Consider an execution interface for a dyadic reductor (\autoref{alg:reductor}).
As stated in \autoref{ss:boxes}, we refer to all but the first output channels
of the reductor as \textit{auxiliary}.

\begin{itemize}
  \itemsep0em
  \item \texttt{is\_ready()} requires unblocked output channels with the first
        one being able to accept two messages: when the reductor yields the
        result, it sends it to the first channel followed by a segmentation
        mark. Messages in both input channels are also required for the box to
        start;
  \item \texttt{fetch()} gets messages from both input channels, for reduction
        terms $a$ and $b_i$ in \eqref{eq:reduction} respectively;
  \item \texttt{run()} tests the messages for being segmentation marks first. A
        $\sigma_k$ in place of $m_a$ results in reduction of the empty sequence
        and, adjusted appropriately, the segmentation mark is sent to the
        auxiliary channels. On the other hand, a $\sigma_k$ in place of $m_b$
        indicates the end of the reduction sequence and results in sending the
        $m_a$ (i.e. the final result of the reduction) to the first output
        channel followed by the appropriately adjusted segmentation mark;
  \item \texttt{commit()} implements the continuation mechanism: a partial
        result (or \textit{accumulator}) of reduction plays the role of a
        continuation message. The method pushes it back to the first input
        channel and sends out auxiliary messages, if any.
\end{itemize}

\begin{algorithm}
  \input{algorithms/boxes_definitions}
  \method{\isReady{}}{

      \Properties{\inputs \tcc*[h]{array of input ports} \\
        \outputs \tcc*[h]{array of output ports}} \;

      $R_i$ = \bool{does \inputs have non-empty ports} \;

      $R_o$ = \bool{is each port in \outputs$[1..n_o]$ unblocked} \;

      \Indp \and \bool{can \outputs$[0]$ accept two messages} \;
    

      \Indm \Return{$(R_i, R_o)$}
  }
  \;
  \method{\Fetch{}}{
      \Properties{\inputs \tcc*[h]{array of input ports}} \;

      $m_a$ = {\inputs}$[0]$.\get{} \;

      $m_b$ = {\inputs}$[1]$.\get{}  \;
      
      \Return{$(m_a, m_b)$}
  }
  \;
  \method{\Commit{\result}}{
      \Properties{\inputs \tcc*[h]{array of input ports} \\
        \outputs \tcc*[h]{array of output ports}} \;

    push \result.\accumulator back to {\inputs}$[0]$ \;

    \ForEach{$($\msg, {\outputs}$[k], ~ k>0$ $)$ \inn \result}{
        send \msg to {\outputs}$[k]$ \;
    }
  }
  \;
  \method{\Run{$m_a$, $m_b$}}{

    \Properties{\inputs \tcc*[h]{array of input ports} \\
      \outputs \tcc*[h]{array of output ports} \\
      \function \tcc*[h]{box function}} \;

    \uIf{$m_a$ is $\sigma_k$}{
      $\sigma_s = 
        \begin{cases}
          \sigma_{k+1},~k > 0 \\
          \sigma_0,~k = 0 \\ 
        \end{cases}$ \;

      send $\sigma_s$ to {\outputs}$[1..n_o]$ \;

      \Return{\none}
    } \;

    \If{$m_b$ \is $\sigma_k$}{
      send $m_a$ to {\outputs}$[0]$ \;

      \If{$k > 1$}{
        $\sigma_s = 
          \begin{cases}
          \sigma_{k-1},~k > 1 \\
          \sigma_0,~k = 0 \\ 
          \end{cases}$ \;

        send $\sigma_s$ to {\outputs}$[0]$ \;
      }
      \Return{\none}
    }

    \Return{$($\function, $m_a, m_b)$}
  }
\caption{Execution interface of reductor.}
\label{alg:reductor}
\end{algorithm}

The execution interface of a \textbf{monadic reductors} is defined in a similar
way.

\subsection{Synchronisers}

\subsubsection*{Overview}

First, the runtime component for a synchroniser maintains a number of
\textit{state} objects, one of which is marked as current. Each state object in
turn consists of \textit{transition} objects, each of which imposes a condition
on input messages. The condition consists of three parts:
\begin{itemize}
  \itemsep0em
  \item the \textbf{input channel} from which a message has to be received;
  \item the \textbf{pattern} of labels that has to match the message. Keyword
        \texttt{.else} can be set instead of the pattern to indicate a
        transition that is taken if no other transitions' pattern matched in the
        current transition scope;
  \item a \textbf{predicate}: a Boolean expression that can include labels
        from the pattern and state variables.
\end{itemize}

Each transition can also have the following augmented actions:
\begin{itemize}
  \itemsep0em
  \item to \texttt{set} internal state of store variables with some integer
        values or messages;
  \item to \texttt{send} messages (or their combinations) to output channels;
  \item to \texttt{goto} the new state(s).
\end{itemize}

Within each state the transitions are also grouped in \textit{blocks}, each
having a priority denoted by $P = \overline{1..N_B}$: the smaller the number,
the higher priority of the transitions.

The execution interface of the synchroniser is presented in the sequel.
Pseudo-code listings for the discussed methods can be found in
\autoref{app:sync_algs}.

\subsubsection*{\texttt{is\_ready()}}

A synchroniser is ready to run when the following requirements are satisfied:
\begin{itemize}
  \itemsep0em
  \item there is at least one input message on a channel for which there is at
        least one transition in the current state;
  \item destination channels in \texttt{send} statements associated with the
        potential transitions are unblocked. In other words, a transition
        being taken must not cause message dispatch to a blocked channel.
\end{itemize}


The method \texttt{is\_ready()} tests these requirements. First, it computes a
set of input channels that can trigger a transition from the current state. If
these channels do not have messages, the negative decision is returned.
Otherwise for each of the channels the method collects potential transitions
that have \texttt{send} statements. If at least one \texttt{send} is targeted at
a blocked channel, the input channel is removed from the set. At the end,
if the set of ready input channels is not empty, it is stored for further use
and the synchroniser is marked as ready.%
%
\subsubsection*{\texttt{fetch()}}

The method operates on the set of ready input channels computed previously. When
there are more then one channel in this set, the choice has to be made
non{-}deterministically by some fairness policy. In the implementation the
method counts how many times each input channel was taken, and the choice is
made in favour of the least frequently taken one. Then, message is fetched from
the chosen channel.

\subsubsection*{\texttt{run()}}

Once a message is fetched from some input channel, there may be more then one
transition declared for it. In this case the transition is chosen by the
following procedure:

\begin{itemize}
  \itemsep0em
  \item The transitions are tested by blocks starting from the one of highest
        priority;
  \item For each transition in the block the pattern and predicate are applied
        to the message. If the message passes the test, the transition is
        added to a \textit{valid} set with a \texttt{.else} transition marked
        separately, if any.
  \item Finally, if there are no valid transitions except for the \texttt{.else}
        one, then it is taken. If there are more than one such transitions, the
        least frequently taken one is chosen. If there are no valid transitions
        at all, the next block is tested.
  \item If no transition has been chosen after testing all the blocks, the
        synchroniser drops the message and terminates.
\end{itemize}



Once a transition is taken, its associated actions are performed. There can be a
situation when several new states are specified in the \texttt{goto} statement.
The preferable ones are those in which the synchroniser is immediately ready to
start. In any case, if there are several states to choose, the least frequently
taken one is chosen.


\subsection{Mergers and Copiers}

As stated in \autoref{s:wiring}, connections between a single output port and
several input ones (and vice versa) are supported by vertices of special types:
copiers and mergers.

A \textbf{merger} is a vertex that reads messages from several input channels
and then sends them to a single output channel in no particular order, i.e.
non{-}deterministically. This behaviour is implemented by a synchroniser, which
operates non-deterministically when more than one transition is available:%
%
\begin{center}\begin{minipage}{0.5\textwidth}
\centering
\begin{alltt}
synch merger (in1, in2, ... | out) \{
  start \{
    on:
      in1 \{ send this => out; \}
      in2 \{ send this => out; \}
      ...
  \}
\}
\end{alltt}
\vspace{-0.5cm}
\end{minipage}\end{center}

The code can easily be extended and generated by the runtime system for any
given number of input channels.

A \textbf{copier}, by contrast, reads messages from a single channel and copies
them to each output ports. The vertex is implemented as follows:

\begin{center}\begin{minipage}{0.5\textwidth}
\centering
\begin{alltt}
synch copier (in | out1, out2, ...) \{
  start \{
    on:
      in \{
        send this => out1,
             this => out2, ...;
      \}
  \}
\}
\end{alltt}
\vspace{-0.5cm}
\end{minipage}\end{center}


\subsection{Extensions}
\label{ss:compound_vertices}

Built-in AstraKahn extensions are constructed from a number of boxes,
synchroniser, and, possibly, preprogrammed executable nets
(\autoref{ss:runtime_cmpts}, \autoref{fig:net_exec}). In this section some
examples of such constructions are presented.

\subsubsection*{Pure Nets}

Recall that pure nets are subnetworks that behave exactly as AstraKahn boxes. In
a sense, this concept generalises a box function by permitting it to be any
subnetwork that meets certain requirements (\autoref{ss:pure_nets}). In terms of
runtime components this generalisation can easily be implemented by extending a
net component with the execution interface of the box category it supposed to
emulate. An example of such a construction is given in \autoref{fig:ptrans_net}.
The only difference between the approach and the conventional execution
interface is that
\texttt{run()} sends a message to the pure net instead of the pool, and
\texttt{commit()} is a net handler that is called when the message is processed
by the pure net.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=8cm]{pure_box}
    }
  \captionof{figure}{A generalised box with a pure network.}
  \label{fig:pure_box}
  \end{minipage}
\end{center}


\subsubsection*{Parallel Boxes}

Once proliferation is applied to a transductor, it is replaced by a net in
\autoref{fig:ptrans_net}. It is a non-standard net since it has two control
methods (denoted by $+/-$ in the figure) that allow the runtime system to change
the proliferation factor. Apart from $n$ parallel copies of the transductor, the
net contains three synchronisers:
\begin{itemize}
  \itemsep0em
  \item \textbf{Splitter} distributes the incoming messages uniformly among the
        parallel transductors;
  \item \textbf{Switch} bypasses messages from the net's input to the Splitter
        until a segmentation mark is received. Then it stops receiving messages
        and sends the segmentation mark and a length of message sequence
        preceding it to the Merger.
  \item \textbf{Merger} forwards the messages from parallel transductors to the
        net's output until their number reaches the sequence length received
        from the Switch. Then the Merger sends a ``releases'' message to the
        Switch for it to continue receiving messages.
\end{itemize}

This protocol serves to keep the messages within their sequences: the net blocks
on receiving a segmentation mark until the preceded sequence is fully processed.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=10cm]{ptransductor_net}
    }
  \captionof{figure}{Parallel transductor.}
  \label{fig:ptrans_net}
  \end{minipage}
\end{center}

\subsubsection*{Synch-Tables}

A synch-table is constructed from an executable net with a single handler and a
synchroniser generated by the compiler (\autoref{fig:synchtab}). The
synchroniser receives an input message, combines values from it according to the
declared pattern, and sends these combinations together with the input message to
the handler. The handler, in turn, dynamically creates synchronisers
corresponding to the received combinations, and sends the message to them. Once
created, synchronisers stay in the net for further possible messages.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=10cm]{synch_table_impl}
    }
  \captionof{figure}{Synch-table.}
  \label{fig:synchtab}
  \end{minipage}
\end{center}


%-------------------------------------------------------------------------------
\newpage
\section{Compiler}
\label{s:compiler}

\deleted{The last module of the AstraKahn implementation that has not been
discussed so far is the compiler.} In this section we consider a transformation
of textual network description into a network of runtime components.

\subsection{Network Construction}

An AstraKahn program consist of a network description, and declaration of
synchronisers and boxes. The languages in which a network and synchronisers are
written are presented in \autoref{ss:lang_net}. The compiler takes the program
and constructs a network of runtime components organised in a tree reflecting
the hierarchy of the network (\autoref{fig:runtime_tree}). Consider this
process in details.

First, the compiler transforms the network description into an abstract syntax
tree (AST):
\begin{center}
  \begin{minipage}[c]{\linewidth}
    \centering
    \captionsetup{type=figure}
    \begin{subfigure}[b]{0.35\textwidth}
        \centering
        \begin{alltt}
\textbf{net} Foo (in | out)
  \textbf{net} Bar (in | out)
  \textbf{synch} S
  \textbf{connect}
    S || B .. D
  end
\textbf{connect}
  (B .. C)\textbackslash || A .. Bar*
\textbf{end}\end{alltt}
        \caption{Network description.}\label{fig:net_example}
    \end{subfigure}
    \begin{subfigure}[b]{0.35\textwidth}
        \centering
        \input{figures/net_ast}
        \caption{AST.}\label{fig:net_ast}
    \end{subfigure}
    \caption{An AstraKahn program transformed into an abstract syntax tree.}
    \label{fig:compile_ast}
  \end{minipage}
\end{center}

Then the following recursive procedure is performed:
\begin{itemize}
  \itemsep0em
  \item Visit root net. Construct scope of vertices that includes synchronisers
        and nested nets from declaration node, and all boxes;
  \item Perform post-order traversal of wiring expression:
        \begin{itemize}
          \itemsep0em
          \item for operand-nodes: construct corresponding runtime component
          (\autoref{s:component_implementation});
          \item for operator-nodes: apply intended wiring pattern to operands
          (\autoref{ss:wire_exp});
        \end{itemize}

        Once a nested net is occurred, recursively apply procedure to it and,
        when its runtime component is build, proceed traversal.
  \item Create net component, include constructed runtime components in it.
\end{itemize}

With regard to the net hierarchy, the procedure visits the nets pre-orderly and
in a `lazy' manner, since nested nets are constructed only if they are used in
wiring expressions.

\subsection{Wiring}
\label{ss:wire_exp}

Once the compiler visits an operator-node of a wiring expression AST, it applies
the wiring pattern to the operand(s). Since the connection between runtime
components is expressed inside the components themselves
(\autoref{ss:runtime_cmpts}), from the compiler standpoint an operand of the
wiring operator is a set of runtime components from the corresponding sub-tree.
For example, in the wiring expression of the net Bar (\autoref{fig:net_ast})
operands of \texttt{||} are sets $\{S\}$ and $\{B, D\}$.

First, for each operand the compiler extracts input and output ports that are
not connected to any components; we refer to the ports as \textit{external}.
Then, if there are identically named external input or output ports, they are
plugged up with corresponding copiers or mergers respectively in order to insure
one-to-one connections between ports.

Finally, the intended connection is applied. For \textit{serial},
\textit{parallel}, or \textit{wrap-around} connections the ports are wired
straightforwardly as described in \autoref{s:wiring}. The case of the
\textit{serial replication} connection is more interesting since the connection
is dynamic: it creates a ``lazy'' pipeline in which stages are constructed and
wired on demand. In what follows a model implementation of the connection based
on executable nets is discussed.

Consider a serial replication connection applied to a stage $A$ with a single
input and output channels. Assume that, regardless of actual implementation of
forward and reversed fixed points, it is possible for the compiler to generate a
net $A^\prime$ that wraps $A$ and has the following properties:
\begin{itemize}
  \itemsep0em
  \item it has a single input port directly connected to $A$, and two output
        ports: ``out'', on which the net forwards messages that are FFP, and
        ``next'', on which the rest of the messages are sent;
  \item a runtime component of $A^\prime$ has a method \texttt{is\_rfp()} that
        tests if the stage is in an RFP state.
\end{itemize}

In order to organise a dynamic pipeline of $A^\prime$, the compiler constructs
an executable network (\autoref{fig:serial_replication}) where the ``out'' ports
of all stages are connected to the global output through a merger, and the
``next'' port of the last stage is connected to the net's handler. Once the
handler receives a message, it creates a new stage of the pipeline, sends the
message to it, and appends the new stage to the head of the pipeline.
Furthermore, on each execution the handler removes RFP stages from the pipeline
are rewire the remaining vertices.

\begin{center}
  \begin{minipage}[c]{.8\linewidth}
    \centering
    \adjustbox{valign=t}{%
      \includegraphics[width=10cm]{serial_replication}
    }
  \captionof{figure}{Serial replication connection.}
  \label{fig:serial_replication}
  \end{minipage}
\end{center}